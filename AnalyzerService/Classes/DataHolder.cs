﻿using UserRecieverService.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AnalyzerService.Classes;

namespace AnalyzerService
{
    public class DataHolder
    {
        private string _rootFolderName;
        Dictionary<string, ExtData> _dataDict;

        public DataHolder(string folderName)
        {
            _rootFolderName = folderName;
            _dataDict = new Dictionary<string, ExtData>();
        }

        public void Append(NewFolder folder)
        {
            if (folder.Files == null || !folder.Files.Any())
                return;

            foreach (var file in folder.Files)
            {
                string ext = file.Ext;
                if (!_dataDict.Keys.Contains(ext))
                {
                    var data = new ExtData();
                    data.Count++;
                    data.TotalSize += file.Size;
                    data.Examples = new List<string>();
                    data.Examples.Add(Path.Combine(folder.FolderPath, file.FileName));
                    _dataDict.Add(ext, data);
                }
                else
                {
                    var data = _dataDict[ext];
                    data.Count++;
                    data.TotalSize += file.Size;
                    if (data.Examples.Count < 2)
                    {
                        data.Examples.Add(Path.Combine(folder.FolderPath, file.FileName));
                    }
                    _dataDict[ext] = data;
                }
            }
        }

        public ExtData Get(string ext)
        {
            return _dataDict[ext] ?? null;
        }

        public List<string> GetAllExtensions()
        {
            return _dataDict.Keys.ToList();
        }

    }
}
