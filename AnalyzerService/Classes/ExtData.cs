﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnalyzerService.Classes
{
    public class ExtData
    {
        public long Count { get; set; }
        public long TotalSize { get; set; }
        public List<string> Examples { get; set; }

        public ExtData(long count, long totalSize, List<string> examples)
        {
            Count = count;
            TotalSize = totalSize;
            Examples = examples;
        }

        public ExtData()
        { }
    }
}
