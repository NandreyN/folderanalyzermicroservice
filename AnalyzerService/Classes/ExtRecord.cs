﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnalyzerService.Classes
{
    public class ExtRecord : ExtData
    {
        public string Ext { get; set; }

        public ExtRecord(string ext, long count, long totalSize, List<string> examples):base(count,totalSize,examples)
        {
            Ext = ext;
        }

        public ExtRecord(string ext, ExtData extData):this(ext, extData.Count, extData.TotalSize, extData.Examples)
        {}
    }
}
