﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using UserRecieverService.Classes;
using System.Net.Http;
using AnalyzerService;
using System.Text;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using AnalyzerService.Classes;
using Microsoft.Extensions.Configuration;

namespace AnalyzerService.Controllers
{
    [Route("api/[controller]")]
    public class AnalyzerController : Controller
    {
        private readonly HttpClient _httpClient;
        private static DataHolder _dataHolder;
        private static DataSizeSuperviser _sizeSuperviser;
        private IConfigurationSection _addresses;

        public AnalyzerController(HttpClient client, IConfiguration config)
        {
            _httpClient = client;
            _addresses = config.GetSection("Addresses");
        }

        private string GetStatisticsAsJson()
        {
            List<ExtRecord> recordList = new List<ExtRecord>();

            var exts = _dataHolder.GetAllExtensions();

            exts.ForEach(x =>
            {
                ExtData extData = _dataHolder.Get(x);
                recordList.Add(new ExtRecord(x, extData));
            });

            return JsonConvert.SerializeObject(recordList).ToString();
        }

        [HttpGet("Ping")]
        public void Ping()
        { }

        // GET api/values
        [HttpGet("Get")]
        public async Task<IActionResult> GetAsync()
        {
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("bearer", Request.Headers["Authorization"].ToString().Trim('"').Replace("bearer ", ""));
            var response = await _httpClient.GetAsync(@_addresses["StorageGetter"]);
            var responseDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(await response.Content.ReadAsStringAsync());

            if (response.IsSuccessStatusCode)
            {
                if (response.StatusCode.Equals(System.Net.HttpStatusCode.OK))
                    return Ok(new { Data = responseDict["data"], isSuccess = true, isFinished = true, ignored = responseDict["ignored"] });
                else if (response.StatusCode.Equals(System.Net.HttpStatusCode.Accepted))
                    return Accepted(new { Data = string.Empty, ErrorMessage = responseDict["errorMessage"], IsSuccess = true, IsFinished = false });
            }

            return BadRequest(new { Data = string.Empty, ErrorMessage = responseDict["errorMessage"], IsFinished = true, IsSuccess = false });
        }

        [HttpPost("Lock")]
        public IActionResult Lock()
        {
            _dataHolder = new DataHolder("");
            _sizeSuperviser = new DataSizeSuperviser();
            return Ok();
        }

        [HttpPost("ExceptionRaised")]
        public async Task<IActionResult> ExceptionRaisedAsync([FromBody]object json)
        {
            if (json == null)
            { return BadRequest("Invalid params"); }

            var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(json.ToString());
            var requestParams = new Dictionary<string, string>()
                {
                    {"token",dict["token"].Trim('"')},
                    {"data", dict["exceptionMessage"]},
                    {"IsSuccess", bool.FalseString }
                };
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("bearer", requestParams["token"]);
            var response = await _httpClient.PostAsync(@_addresses["StorageGetter"], new StringContent(JsonConvert.SerializeObject(requestParams).ToString(), Encoding.UTF8, "application/json"));
            if (!response.IsSuccessStatusCode)
                return BadRequest(new { ErrorMessage = "Error message wasn`t recived", IsSuccess = false });

            return Ok();
        }

        [HttpPost("Finalyze")]
        public async Task<IActionResult> FinalyzeAsync([FromBody]object json)
        {
            if (json == null)
            {
                return BadRequest("Invalid params , token required");
            }

            var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(json.ToString());
            if (!dict.Keys.Contains("token"))
            {
                return BadRequest("Invalid params , token required");
            }

            string data = GetStatisticsAsJson();
            var requestParams = new Dictionary<string, string>()
                {
                    {"token",dict["token"].Trim('"')},
                    {"data", data},
                    {"isSuccess", bool.TrueString },
                    {"ignored", dict["ignored"] }
                };

            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("bearer", requestParams["token"]);
            var response = await _httpClient.PostAsync(@_addresses["StorageGetter"], new StringContent(JsonConvert.SerializeObject(requestParams).ToString(), Encoding.UTF8, "application/json"));
            if (!response.IsSuccessStatusCode)
                return BadRequest(new { ErrorMessage = "Confirmation not recived", IsSuccess = false });

            return Ok();
        }

        private string Get(string ext)
        {
            ExtData data = _dataHolder.Get(ext);
            if (data == null)
                return "Invalid extension";

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("Extension {0}: ", ext);
            sb.AppendFormat("Total count: {0}, Total size: {1} kb,", data.Count, data.TotalSize / 1024);
            if (data.Examples.Count == 1)
            {
                sb.AppendFormat(" Examples: {0}", data.Examples[0]);
            }
            else
            {
                sb.AppendFormat(" Examples: {0},{1}", data.Examples[0], data.Examples[1]);
            }
            return sb.ToString();
        }

        // POST api/values
        [HttpPost("")]
        public IActionResult Post([FromBody]object json)
        {
            if (json == null) return BadRequest();
            string s = json.ToString();
            var folderList = JsonConvert.DeserializeObject<List<NewFolder>>(s);

            foreach (var folder in folderList)
            {
                _dataHolder.Append(folder);
                _sizeSuperviser.AddBytes(Encoding.ASCII.GetByteCount(s));
            }

            return Ok();
        }
    }
}
