﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Client.Models;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using System.ComponentModel;
using System.Timers;
using System.Net;

namespace Client.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfigurationSection _configAddressSection;
        private readonly HttpClient _httpClient;


        public HomeController(IConfiguration config, HttpClient client)
        {
            _configAddressSection = config.GetSection("Addresses");
            _httpClient = client;
        }

        public IActionResult Index()
        {
            ViewData["SendFolderAddress"] = _configAddressSection["SendFolderAddress"];
            ViewData["GetResultAddress"] = _configAddressSection["GetResultAddress"];
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public async Task<IActionResult> Scan(FolderPathModel model)
        {
            HttpContent requestContext = new StringContent(JsonConvert.SerializeObject(new { path = model.Path }).ToString(),
                Encoding.UTF8, "application/json");
            return new JsonResult(await _httpClient.PostAsync(_configAddressSection["SendFolderAddress"], requestContext));
        }


        [HttpPost]
        public async Task<JsonResult> GetTokenAsync(string path)
        {
            HttpContent requestContext = new StringContent(JsonConvert.SerializeObject(new { path = path }).ToString(),
              Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(_configAddressSection["SendFolderAddress"], requestContext);
            var responseDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(await response.Content.ReadAsStringAsync());

            Response.StatusCode = (!response.IsSuccessStatusCode) ? (int)response.StatusCode : (int)HttpStatusCode.OK;
            return Json(new { token = responseDict["token"], success = responseDict["isSuccess"], errorMessage = responseDict["errorMessage"] });
        }

        [HttpGet]
        public async Task<JsonResult> GetResultAsync(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new { success = false, finished = true });
            }
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("bearer", token);
            var response = _httpClient.GetAsync(_configAddressSection["GetResultAddress"]);


            if (!response.Result.IsSuccessStatusCode)
            {
                Response.StatusCode = (int)response.Result.StatusCode;
                return Json(new { success = false, finished = true, errorMessage = "Error detected, try again" });
            }

            string s = await response.Result.Content.ReadAsStringAsync();
            Dictionary<string, string> bodyDict = null;
            try
            {
                bodyDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(s);
            }
            catch (Exception e)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(new
                {
                    success = false,
                    finished = true,
                    errorMessage = e.Message
                });
            }

            if (bodyDict == null)
            {
                Response.StatusCode = (int)HttpStatusCode.NoContent;
                return Json(new
                {
                    success = false,
                    finished = true,
                    errorMessage = "Please , try again"
                });
            }

            Response.StatusCode = (int)HttpStatusCode.OK;
            return Json(new
            {
                success = bodyDict["isSuccess"],
                finished = bodyDict["isFinished"],
                data = (bodyDict.Keys.Contains("data")) ? bodyDict["data"] : null,
                errorMessage = (bodyDict.Keys.Contains("errorMessage")) ? bodyDict["errorMessage"] : null,
                ignored = (bodyDict.Keys.Contains("ignored")) ? bodyDict["ignored"] : null
            });
        }
        private List<ExtensionsModel> ParseModel(string extData)
        {
            return JsonConvert.DeserializeObject<List<ExtensionsModel>>(extData);
        }

        [HttpPost]
        public ActionResult DisplayResult(string extData, string ignored)
        {
            if (extData == null)
                return PartialView("TableView", null);
            if (ignored == null)
                ignored = "null";

            List<ExtensionsModel> extModelCollection = null;
            try
            {
                extModelCollection = ParseModel(extData);
            }
            catch (Exception e)
            {
                Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                return Json(new { errorMessage = e.Message });
            }

            Response.StatusCode = (int)HttpStatusCode.OK;
            return PartialView("TableView", new VisualPartModel() { extensionsModel = extModelCollection, ignoredFolders = JsonConvert.DeserializeObject<List<string>>(ignored) });
        }

        [HttpPost]
        public ActionResult SortBy(int sortId, string data, bool inverse, string ignored)
        {
            List<string> ignoredAsList = JsonConvert.DeserializeObject<List<string>>(ignored);
            List<ExtensionsModel> model = ParseModel(data);
            if (model == null)
                return PartialView("TableView", null);
            switch (sortId)
            {
                case 0:
                    if (!inverse)
                        model.Sort((x, y) => string.Compare(x.Ext, y.Ext));
                    else
                        model.Sort((x, y) => string.Compare(y.Ext, x.Ext));

                    break;
                case 1:
                    if (!inverse)
                        model.Sort((x, y) =>
                        {
                            if (x.Count < y.Count)
                                return -1;
                            if (x.Count > y.Count)
                                return 1;
                            return 0;
                        });
                    else
                        model.Sort((x, y) =>
                        {
                            if (y.Count < x.Count)
                                return -1;
                            if (y.Count > x.Count)
                                return 1;
                            return 0;
                        });
                    break;
                case 2:
                    if (!inverse)
                        model.Sort((x, y) =>
                        {
                            if (x.TotalSize < y.TotalSize)
                                return -1;
                            if (x.TotalSize > y.TotalSize)
                                return 1;
                            return 0;
                        });
                    else
                        model.Sort((x, y) =>
                        {
                            if (y.TotalSize < x.TotalSize)
                                return -1;
                            if (y.TotalSize > x.TotalSize)
                                return 1;
                            return 0;
                        });
                    break;

            }
            return PartialView("TableView", new VisualPartModel() { extensionsModel = model, ignoredFolders = ignoredAsList });
        }

        [HttpGet]
        public async Task PingAsync()
        {
            var response = await _httpClient.GetAsync(_configAddressSection["PingAddress"]);
        }
    }
}
