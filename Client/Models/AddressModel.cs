﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Client.Models
{
    public class AddressModel
    {
        public string SendFolderAddress { get; set; }
        public string GetResultAddress { get; set; }
    }
}
