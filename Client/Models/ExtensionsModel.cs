﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Client.Models
{
    public class ExtensionsModel
    {
        public string Ext { get; set; }
        public long Count { get; set; }
        public long TotalSize { get; set; }

        public List<string> Examples { get; set; }
    }
}
