﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Client.Models
{
    public class VisualPartModel
    {
        public IEnumerable<ExtensionsModel> extensionsModel { get; set; }
        public List<string> ignoredFolders { get; set; }
    }
}
