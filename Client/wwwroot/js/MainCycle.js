﻿function showAndHide(selector, text) {
    $(selector).text(text);
    $(selector).show(500);
    setTimeout(function () { $(selector).hide(500); }, 2000);
}

$(document).ready(function () {
    $('#info').hide();
    $('#danger').hide();
    $('#success').hide();
    $('#warning').hide();
    $('#loaderBar').hide();

    $('#folderField').focus(function () {
        $(this).val('');
    });

    function display(jsonData, ignored) {
        $.ajax({
            url: '/Home/DisplayResult',
            data: { extData: jsonData, ignored: ignored },
            cache: false,
            type: "POST",
            dataType: "html",
            success: function (data, textStatus, XMLHttpRequest) {
                $('#resultTable').html(data);
                $('#resultTable').show();

                $('#loaderBar').hide();
                showAndHide('#success', "Success");
            },
            error: function (flData) {
                $('#loaderBar').hide();
                showAndHide('#danger', failData.errorMessage);
            }
        });
    }


    $('#scanButton').click(function () {
        isFinished = false;
        $('#resultTable').hide();
        $('#loaderBar').show();
        showAndHide('#info', "Processing");
        var dataToDisplay = null;
        window.globToken = '';
        $(this).prop("disabled", true);


        $.ajax({
            url: '/Home/GetTokenAsync',
            data: { path: $('#folderField').val() },
            type: "POST",
            success: function (tokenResponseData) {
                function getRes() {
                    $.ajax({
                        url: '/Home/GetResultAsync',
                        type: 'GET',
                        dataType: 'json',
                        cache : false,
                        data: { token: tokenResponseData.token },
                        success: function (successResultResponseData) {
                            if (successResultResponseData.finished == "false")
                                setTimeout(function () { getRes(); }, 2000);
                            else {
                                display(successResultResponseData.data, successResultResponseData.ignored);
                                window.globTableData = successResultResponseData.data;
                                window.ignored = successResultResponseData.ignored;
                                $('#scanButton').prop("disabled", false);
                            }
                        },
                        error: function (failResultResponseData) {
                            showAndHide('#danger', failResultResponseData.errorMessage);
                            $('#loaderBar').hide();
                            $('#scanButton').prop("disabled", false);
                        }
                    });
                }

                getRes();
            },
            error: function (failData) {
                showAndHide('#danger', failData.responseJSON["errorMessage"]);
                $('#loaderBar').hide();
                $('#scanButton').prop("disabled", false);
            }
        });
    });
});