﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StorageController.Classes
{
    public class StatisticsDataContext:DbContext
    {
        public StatisticsDataContext(DbContextOptions<StatisticsDataContext> options) : base(options)
        {

        }
        public DbSet<StatisticsEntity> Statistics { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<StatisticsEntity>().ToTable("StatisticsData");
        }
    }
}
