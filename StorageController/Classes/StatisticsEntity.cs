﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace StorageController.Classes
{
    [Table("StatisticsData")]
    public class StatisticsEntity
    {
        [Column("Token")]
        public string Token { get; set; }
        [Column("Data")]
        public string Data { get; set; }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("Id")]
        public int Id { get; set; }

        [Column("IsFinal")]
        public string IsFinal { get ; set; }

        [Column("IsSuccess")]
        public string IsSuccess { get; set; }

        [Column("Ignored")]
        public string Ignored { get; set; }
    }
}
