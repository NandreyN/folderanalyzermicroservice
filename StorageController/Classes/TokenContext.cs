﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Threading.Tasks;
using UserRecieverService.Classes;
using Microsoft.EntityFrameworkCore;
using StorageController.Classes;

namespace Storage.Classes
{
    public class TokenContext : DbContext
    {
        public TokenContext(DbContextOptions<TokenContext> options) : base(options)
        {
           
        }

        public DbSet<Token> Tokens { get; set; }
        public DbSet<StatisticsEntity> Statistics { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Token>().ToTable("Table");
            modelBuilder.Entity<StatisticsEntity>().ToTable("StatisticsData");
        }
    }

}
