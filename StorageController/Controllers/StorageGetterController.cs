﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Storage.Classes;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using StorageController.Classes;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Hangfire;

namespace StorageService.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class StorageGetterController : Controller
    {
        private readonly TokenContext _tokenContext;
        private readonly HttpClient _httpClient;
        private readonly IConfigurationSection _addresses;
        public StorageGetterController(TokenContext context, HttpClient http, IConfiguration config)
        {
            _tokenContext = context;
            _httpClient = http;
            _addresses = config.GetSection("Addresses");
        }

        [Authorize]
        [HttpGet("")]
        public async Task<IActionResult> GetDataAsync()
        {
            // Here : return notflund if record doesn`t exist | accepted if calulations are not completed
            // Define completion : bool variable in data base? Empty Data cell? 

            string token = ExtractHeaderToken();
            if (string.IsNullOrEmpty(token))
                return BadRequest(new { ErrorMessage = "Empty token", IsSuccess = false });

            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("bearer", token);
            var response = _httpClient.PostAsync(@_addresses["IsAvailable"], null);
            bool isAvailable = false;
            string boolStr = await response.Result.Content.ReadAsStringAsync();
            bool.TryParse(boolStr, out isAvailable);
            if (!isAvailable)
            {
                _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("bearer", token);
                return NotFound(new { ErrorMessage = "Token is expired/isn`t registered", IsSuccess = false, IsFinished = true });
            }

            StatisticsEntity entity = _tokenContext.Statistics.FirstOrDefault(x => x.Token.ToString() == token);
            if (entity == null)
            {
                return NotFound(new { Token = "Cannot find record with that token", IsSuccess = false, IsFinished = true });
            }

            if (entity.Data == string.Empty && entity.IsFinal == bool.FalseString)
            {
                return Accepted(new { ErrorMessage = "Processing", IsSuccess = true, IsFinished = false });
            }

            if (entity.IsSuccess.Equals(bool.FalseString))
            {
                //return Forbid(JwtBearerDefaults.AuthenticationScheme);
                await _httpClient.DeleteAsync(@_addresses["Unregister"]);
                Response.StatusCode = 422;
                return BadRequest(new { Data = "", ErrorMessage = entity.Data, isFinished = true, isSuccess = false });
            }

            await _httpClient.DeleteAsync(@_addresses["Unregister"]);
            return Ok(new { Data = entity.Data, isSuccess = true, isFinished = true, ignored = entity.Ignored });

        }


        [Authorize]
        [HttpPost("")]
        public async Task<IActionResult> PushDataAsync([FromBody]object requestData)
        {
            if (requestData == null)
                return BadRequest(new { ErrorMessage = "Invalid params", IsSuccess = false });
            var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(requestData.ToString());
            if (!dict.Keys.Contains("token") || !dict.Keys.Contains("data"))
                return BadRequest(new { ErrorMessage = "Invalid params", IsSuccess = false });

            // compare header token with current  (later)
            string headerToken = ExtractHeaderToken();
            if (!headerToken.Equals(dict["token"]))
            {
                return BadRequest(new { ErrorMessage = "Tokens are not equal", IsSuccess = false });
            }

            var stat = _tokenContext.Statistics.FirstOrDefault(x => x.Token.ToString() == dict["token"]);
            if (stat == null)
            {
                return NotFound(new { ErrorMessage = "No record in db, can`t apply data", IsSuccess = false });
            }
            _tokenContext.Statistics.Remove(stat);
            await _tokenContext.SaveChangesAsync();

            stat.IsFinal = bool.TrueString;
            stat.Data = dict["data"];
            stat.IsSuccess = dict["isSuccess"];
            stat.Ignored = dict["ignored"];
            _tokenContext.Statistics.Add(stat);

            await _tokenContext.SaveChangesAsync();
            return Ok();
        }

        private string ExtractHeaderToken()
        {
            return Request.Headers["Authorization"].ToString().Trim('"').Replace("bearer ", "");
        }
    }
}