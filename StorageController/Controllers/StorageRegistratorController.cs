﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Storage.Classes;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using Newtonsoft.Json;
using System.Net.Http;
using StorageController.Classes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace Storage.Controllers
{
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class StorageRegistratorController : Controller
    {
        private readonly TokenContext _tokenContext;
        private readonly IConfiguration _config;
        public StorageRegistratorController(TokenContext tokenContext, IConfiguration configuration)
        {
            _tokenContext = tokenContext;
            _config = configuration;
        }

        [AllowAnonymous]
        [HttpGet("Ping")]
        public void Ping()
        { }

        [AllowAnonymous]
        [HttpGet("Register")]
        public async Task<IActionResult> RegisterAsync()
        {
            string val = _config.GetSection("AppSettings:Key").Value;
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(val));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            DateTime expiry = DateTime.Now.AddMinutes(60);
            var token = new JwtSecurityToken("me",
                "you",
                null,
                expires: expiry,
                signingCredentials: creds);

            string newTokenStringFormat = new JwtSecurityTokenHandler().WriteToken(token);

            int newTokenIndex = (_tokenContext.Tokens != null && _tokenContext.Tokens.Any()) ? _tokenContext.Tokens.Max(x => x.Id) : 0;
            newTokenIndex++;

            _tokenContext.Tokens.Add(new Storage.Classes.Token
            {
                Id = newTokenIndex,
                Text = newTokenStringFormat,
                Expires = expiry
            });

            newTokenIndex = (_tokenContext.Statistics != null && _tokenContext.Statistics.Any()) ? _tokenContext.Statistics.Max(x => x.Id) : 0;
            newTokenIndex++;

            _tokenContext.Statistics.Add(new StatisticsEntity
            {
                Data = "",
                Id = newTokenIndex,
                Token = newTokenStringFormat,
                IsFinal = bool.FalseString,
                IsSuccess = bool.TrueString
            });

            try
            {
                await _tokenContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            return Ok(new {Token = newTokenStringFormat, IsSuccess = true, ErrorMessage = ""});
        }


        [HttpPost("Isavailable")]
        public IActionResult IsAvailable()
        {
            ///<summary>
            ///This method checks if token is expired and returns result with bool parameter as json dict
            ///</summary>
            string tkn = Request.Headers["Authorization"].ToString().Trim('"').Replace("bearer ", "");
            if (string.IsNullOrEmpty(tkn))
                return BadRequest("Invalid params");

            var token = _tokenContext.Tokens.FirstOrDefault(x => x.Text.ToString() == tkn);
            return token != null && DateTime.Compare(token.Expires, DateTime.Now) >= 0 ? Json(true) : Json(false);
        }

        [HttpDelete("Unregister")]  
        public async Task<IActionResult> UnregisterAsync()
        {
            string tkn = Request.Headers["Authorization"].ToString();
            if (string.IsNullOrEmpty(tkn))
                return BadRequest("Invalid params");

            tkn = tkn.Replace("bearer ", "");
            var token = _tokenContext.Tokens.FirstOrDefault(x => x.Text.ToString() == tkn);
            if (token == null) return Ok();
            _tokenContext.Tokens.Remove(token);

            var record = _tokenContext.Statistics.FirstOrDefault(x => x.Token.ToString() == tkn);
            if (record == null) return Ok();
            _tokenContext.Statistics.Remove(record);
            await _tokenContext.SaveChangesAsync();
            return Ok();
        }
    }
}
