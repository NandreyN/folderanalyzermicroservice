﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Text;

namespace UserRecieverService.Classes
{
    public abstract class AbstractFolder
    {
        [JsonIgnore]
        protected int _id;
        [JsonIgnore]
        protected IEnumerable<int> _subfolderIds;
        [JsonProperty]
        protected IEnumerable<File> _files;
        [JsonProperty]
        protected string _folderPath;

        [JsonIgnore]
        public  int Id => _id;
        [JsonIgnore]
        public IEnumerable<int> SubfolderIds => _subfolderIds;
        [JsonIgnore]
        public IEnumerable<File> Files => _files;
        [JsonIgnore]
        public string FolderPath => _folderPath;

        public AbstractFolder()
        {
            _id = -1;
            _subfolderIds = null;
            _files = null;
            _folderPath = string.Empty;
        }

        public AbstractFolder(int id, string folderPath)
        {
            _id = id;
            _subfolderIds = null;
            _files = null;
            _folderPath = folderPath;
        }

        public abstract void SetContent(IEnumerable<int> subFolderIds, IEnumerable<File> files);

        public abstract void Enumerate(IVisitor visitor);
    }
}
