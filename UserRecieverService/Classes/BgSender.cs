﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Microsoft.Extensions.Configuration;

namespace UserRecieverService.Classes
{
    public class BackGroundSender
    {
        private readonly HttpClient _httpClient;
        private FolderKeeper _keeper;
        private readonly IConfigurationSection _addressesSection;

        public BackGroundSender(HttpClient client, IConfiguration config)
        {
            _httpClient = client;
            _addressesSection = config.GetSection("Addresses");
        }

        public async Task SendAsync(string path, string token)
        {
            try
            {
                _keeper = new FolderKeeper(new NewFolder(0, path));
                await SendCollectionAsync(token);
            }
            catch (Exception e)
            {
                var reqParams = new Dictionary<string, string>()
                {
                    { "token",token},
                    { "exceptionMessage",e.Message}
                };
                await _httpClient.PostAsync(@_addressesSection["ErrorReport"], new StringContent(JsonConvert.SerializeObject(reqParams).ToString(), Encoding.UTF8, "application/json"));
                return;
            }
        }

        private async Task SendCollectionAsync(string token)
        {
            ///<summary>
            /// Uses Lock Post Request to notify Analyzer that new transmittion is started
            /// Folders are being sent in chunks
            /// Finalyze Post Request to finish transmittion 
            ///</summary>

            var response = await _httpClient.PostAsync(@_addressesSection["Lock"], new StringContent(string.Empty, Encoding.UTF8, "application/json"));
            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException("bad request of another service");
            }

            int chunkSize = (_keeper != null) ? _keeper.GetChunkSize() : 0;
            while (_keeper.Size() > 0)
            {
                response = await _httpClient.PostAsync(@_addressesSection["Transmit"], new StringContent(ChunkCreate(chunkSize), Encoding.UTF8, "application/json"));
                if (!response.IsSuccessStatusCode)
                {
                    throw new HttpRequestException("Analyzer appending exception");
                }
            }

            var tokenDict = new Dictionary<string, string> { { "token", token }, { "ignored", JsonConvert.SerializeObject(_keeper.Ignored)} };
            response = await _httpClient.PostAsync(_addressesSection["Finalyze"], new StringContent(JsonConvert.SerializeObject(tokenDict).ToString(), Encoding.UTF8, "application/json"));
            if (!response.IsSuccessStatusCode)
            {
                throw new HttpRequestException("Unable to finalyze analyzer");
            }
        }

        private string ChunkCreate(int chunkSize)
        {
            /// <summary>
            /// Combining several NewFolder content into one string 
            /// </summary>

            var folderList = _keeper.Pop(count: chunkSize).Cast<NewFolder>().ToList();
            if (folderList == null || folderList.Count == 0)
            {
                return string.Empty;
            }

            return JsonConvert.SerializeObject(folderList);
        }
    }
}
