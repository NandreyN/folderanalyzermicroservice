﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserRecieverService.Classes
{
    public class DataSizeSuperviser
    {
        private double _bytes;

        public double MegaBytes => _bytes / 1024 / 1024;

        public DataSizeSuperviser()
        {
            _bytes = 0;
        }
        public void AddBytes(long bytesCount)
        {
            _bytes += bytesCount;
        }
    }
}
