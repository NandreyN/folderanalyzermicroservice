﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace UserRecieverService.Classes
{
    public class File
    {
        [JsonProperty]
        private string _fileName;
        [JsonProperty]
        private string _ext;
        [JsonProperty]
        private long _size;

        [JsonIgnore]
        public string FileName { get => _fileName; }
        [JsonIgnore]
        public string Ext { get => _ext; }
        [JsonIgnore]
        public long Size { get => _size; }

        [JsonConstructor]
        public File(string fileName, string ext, long size)
        {
            _fileName = fileName;
            _ext = ext;
            _size = size;
        }

        public File(FileInfo fileInfo)
        {
            _fileName = fileInfo.Name;
            _ext = fileInfo.Extension; // with dot 
            _size = fileInfo.Length;
        }
    }
}
