﻿using CounterMircoservice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserRecieverService.Classes
{
    public class FolderKeeper : IKeeper
    {
        private Dictionary<int, AbstractFolder> _data;
        private List<string> _ignored;

        public List<string> Ignored { get => _ignored;}

        public FolderKeeper(AbstractFolder fldr)
        {
            if (fldr != null)
            {
                var visitor = new LocalVisitor();
                visitor.Reset();
                visitor.Visit(fldr, fldr.Id);
                _data = visitor.GetResult();
                _ignored = visitor.GetIgnoredList();
            }
            else
            {
                _ignored = new List<string>();
                _data = new Dictionary<int, AbstractFolder>();
            }
        }

        public void Add(int id, AbstractFolder folder)
        {
            _data[id] = folder ?? throw new ArgumentException();
        }

        public AbstractFolder Get(int id)
        {
            return _data.Keys.Contains(id) ? _data[id] : null;
        }

        public List<int> GetIdCollection()
        {
            return _data.Keys.ToList();
        }

        public int GetChunkSize()
        {
            return 4;
        }

        public IEnumerable<AbstractFolder> Pop(int count)
        {
            if (count > _data.Count)
                count = _data.Count;

            List<AbstractFolder> folders = new List<AbstractFolder>(count);
            List<int> toRemove = new List<int>(count);
            foreach (var item in _data.Take(count))
            {
                folders.Add(item.Value);
                toRemove.Add(item.Key);
            }

            foreach (var key in toRemove)
                _data.Remove(key);

            return folders;
        }

        public int Size()
        {
            return _data.Count;
        }
    }
}
