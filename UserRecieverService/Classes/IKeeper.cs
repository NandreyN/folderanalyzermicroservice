﻿using CounterMircoservice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserRecieverService.Classes
{
    interface IKeeper
    {
        void Add(int id, AbstractFolder folder);
        AbstractFolder Get(int id);
    }
}
