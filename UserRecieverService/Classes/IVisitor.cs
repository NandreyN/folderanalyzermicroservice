﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserRecieverService.Classes
{
    public interface IVisitor
    {
        void Visit(AbstractFolder folder, int id);
    }
}
