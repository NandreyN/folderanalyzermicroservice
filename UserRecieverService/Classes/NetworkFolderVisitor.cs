﻿using UserRecieverService.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Security.AccessControl;

namespace UserRecieverService.Classes
{
    public class LocalVisitor : IVisitor
    {
        private static int _id = 1;
        private static Dictionary<int, AbstractFolder> _dict = null;
        private static Dictionary<string, int> _srcPathDict = null;
        private static List<string> _ignoredFolders = null;

        public LocalVisitor()
        {
            _dict = new Dictionary<int, AbstractFolder>();
            _srcPathDict = new Dictionary<string, int>();
            _ignoredFolders = new List<string>();
        }

        public void Reset()
        {
            _id = 1;
            _srcPathDict = new Dictionary<string, int>();
        }

       
        public void Visit(AbstractFolder folder, int id)
        {
            var subFolders = new List<int>();
            var files = new List<File>();
            //E:\System Volume Information
           
            DirectoryInfo dir = new DirectoryInfo(folder.FolderPath);

            IEnumerable<DirectoryInfo> subDirs = null;
            try
            {
                subDirs = dir.EnumerateDirectories().ToList();
            }
            catch (UnauthorizedAccessException e)
            {
                _ignoredFolders.Add(folder.FolderPath);
                return;
            }
            
            var folderSelfFiles = dir.EnumerateFiles();
            foreach (var fl in folderSelfFiles)
            {
                var currFile = new File(fl);
                files.Add(currFile);
            }

            
            foreach (var fldr in subDirs)
            {
                subFolders.Add(_id);
                _srcPathDict[fldr.FullName] = _id;
                ++_id;
            }
            folder.SetContent(subFolders, files);
            _dict[folder.Id] = folder;

            foreach (var fldr in subDirs)
            {
                Visit(new NewFolder(_srcPathDict[fldr.FullName], fldr.FullName), _srcPathDict[fldr.FullName]);
            }
        }

        public Dictionary<int, AbstractFolder> GetResult()
        {
            return _dict ?? new Dictionary<int, AbstractFolder>();
        }

        public List<string> GetIgnoredList()
        {
            return _ignoredFolders.Count > 0 ? _ignoredFolders : null;
        }
    }
}