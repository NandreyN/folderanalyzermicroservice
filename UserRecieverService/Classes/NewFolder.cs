﻿using CounterMircoservice;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserRecieverService.Classes
{
    public class NewFolder:AbstractFolder
    {

        public NewFolder():base()
        { }

        public NewFolder(int id, string folderPath):base(id,folderPath)
        {}

        public override void SetContent(IEnumerable<int> subFolderIds, IEnumerable<File> files)
        {
            _subfolderIds = subFolderIds;
            _files = files;
        }

        public override void Enumerate(IVisitor visitor)
        {
            visitor.Visit(this, _id);
        }
    }
}
