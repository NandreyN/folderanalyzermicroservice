﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserRecieverService.Classes;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using Hangfire;
using System.Net;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.EntityFrameworkCore;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.IdentityModel.Protocols;

namespace UserRecieverService.Controllers
{
    [Route("api/[controller]")]
    public class UserRecieverController : Controller
    {
        private readonly HttpClient _httpClient;
        private readonly IConfigurationSection _addressesSection, _pingSection;
        public UserRecieverController(HttpClient client, IConfiguration config)
        {
            _httpClient = client;
            _addressesSection = config.GetSection("Addresses");
            _pingSection = config.GetSection("Ping");
        }

        [HttpGet]
        public async Task PingAsync()
        {
            var rsp = await _httpClient.GetAsync(@_addressesSection["StorageControllerPing"]);
            rsp = await _httpClient.GetAsync(@_addressesSection["GetControllerPing"]);
        }

        // POST api/userreciever
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody]object jsonString)
        {
            ///<summary>
            /// That request hadles user input. Folder path is passed inside ductionary under "path" keyword.
            /// Than locking request is performed in order to notify analyzer that data is quering
            /// Perform request to Register Controller, which creates validation token for current request and creates record in database.
            /// Than token is returned to client and Folder initialization is started.
            /// Data transmittion is pushed into Background via HangFire framework
            ///</summary>
            ///
            if (jsonString == null || string.IsNullOrEmpty(jsonString.ToString()))
            {
                return NotFound(new { ErrorMessage = "Empty request", IsSuccess = false, Token = "" });
            }

            var paramDict = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonString.ToString());
            string folderPath = (paramDict != null && paramDict.Keys.Contains("path")) ? paramDict["path"] : null;

            if (string.IsNullOrEmpty(folderPath) || !Directory.Exists(folderPath))
            {
                return NotFound(new { ErrorMessage = "Unknown folder", IsSuccess = false, Token = "" });
            }

            var rsp = await _httpClient.GetAsync(@_addressesSection["Register"]);
            string content = await rsp.Content.ReadAsStringAsync();
            var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(content);
            if (dict == null || Boolean.Parse(dict["isSuccess"]).Equals(false))
            {
                return BadRequest(new { ErrorMessage = "Token wasn`t recived", IsSuccess = false, Token = "" });
            }

            BackgroundJob.Enqueue(() => ContinueAfterTokenRecieved(folderPath, dict["token"]));
            return Ok(content);
        }

        public static void ContinueAfterTokenRecieved(string folderPath, string token)
        {
            ///<summary>
            ///Creates Folder object , initializes FodlerKeeper
            ///Initializes data transmittion
            ///</summary>
            ///
            BackgroundJob.Enqueue<BackGroundSender>(x => x.SendAsync(folderPath, token));
        }
    }
}